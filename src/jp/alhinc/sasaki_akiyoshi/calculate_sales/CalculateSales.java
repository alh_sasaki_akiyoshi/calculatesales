package jp.alhinc.sasaki_akiyoshi.calculate_sales;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {
		BufferedReader br = null;
		BufferedReader brSales = null;
		HashMap<String, String> storeMap = new HashMap <String, String>(); //key:店番 value:支店名
		HashMap<String, Long> salesMap = new HashMap <String, Long>(); //key:店番 value:売上
		if(!inputFile(args[0],"branch.lst",br,brSales,storeMap,salesMap)){
			return;
		}
		try { //売り上げファイル読み込み保持&集計

			File Files = new File(args[0]);
			String[] salesFileList = Files.list();
			ArrayList<String> salesDataArray = new ArrayList<String>();

			for (int i = 0; i < salesFileList.length ; i++) {
				if(salesFileList[i].matches("\\A[0-9]{8}.rcd\\Z")){
					salesDataArray.add(salesFileList[i]);
				}
			}
			ArrayList<String> salesDataArray2 = new ArrayList<String>();
			for(int i = 0; i < salesDataArray.size() - 1 ;i++){
				String strSalesFilesNum1 = salesDataArray.get(i).replace(".rcd","");
				String strSalesFilesNum2 = salesDataArray.get(i + 1).replace(".rcd","");
				int intSalesFilesNum1 = Integer.parseInt(strSalesFilesNum1);
				int intSalesFilesNum2 = Integer.parseInt(strSalesFilesNum2);
				if(strSalesFilesNum1.matches("^[0-9]{8}$") && intSalesFilesNum2 - intSalesFilesNum1 == 1){
					salesDataArray2.add(salesFileList[i]);
				} else {
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}
			}
			for(int i = 0; i<= salesDataArray2.size() ;i++){
				ArrayList<String> salesArrayList = new ArrayList<String>();
				File salesFiles = new File(args[0] , salesFileList[i]);
				FileReader frSales = new FileReader(salesFiles);
				brSales = new BufferedReader(frSales);

				String lineSales;
				while ((lineSales = brSales.readLine()) != null){
					salesArrayList.add(lineSales);
				}
				if(salesArrayList.size() != 2){
					System.out.println(salesFileList[i] + "のフォーマットが不正です");
					return;
				}
				if(!salesArrayList.get(1).matches("[0-9]{1,}$")){
					System.out.println("予期せぬエラーが発生しました4");
					return;
				}
				if(!salesArrayList.get(1).matches("^\\d{1,10}$")){
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				long longSales  = Long.parseLong(salesArrayList.get(1));
				Long codeLap;
				if (storeMap.containsKey(salesArrayList.get(0))){
					codeLap = salesMap.get(salesArrayList.get(0)) + longSales;
					salesMap.put(salesArrayList.get(0), codeLap);
				} else {
					System.out.println(salesFileList[i] + "のフォーマットが不正です");
					return;
				}
			}
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました5");
			return;
		} finally {
			if (brSales != null){

				try {
					brSales.close();

				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました6");
					return;
				}
			}
		}
		if(!outputFile(args[0], "branch.out", storeMap, salesMap)){
			return;
		}
	}


	public static boolean outputFile(String dirPass, String fileName, Map<String,String> codes,
			Map<String,Long> names) {
		BufferedWriter bwOutPut = null;
		try { //集計出力
			File outPutFile = new File(dirPass, fileName);
			FileWriter fwOutPut = new FileWriter(outPutFile);
			bwOutPut = new BufferedWriter(fwOutPut);
			PrintWriter pwOutPut = new PrintWriter(bwOutPut);
			for(Map.Entry<String, String> codeStore : codes.entrySet()) {

				for(Map.Entry<String, Long> totalSales  : names.entrySet()) {
					if (codeStore.getKey().equals(totalSales.getKey())){
						pwOutPut.println(codeStore.getKey() + "," + codeStore.getValue() + "," + totalSales.getValue());
					}
				}
			}
		}catch (IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (bwOutPut != null){
				try {
					bwOutPut.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		} return true;
	}

	public static boolean inputFile(String codeDirPass, String inFileName, BufferedReader brStore,BufferedReader brSale, Map<String,String> branchMap,
			Map<String,Long> saleMap){
		try { //支店定義ファイル読み込み保持
			if((0 < codeDirPass.length()) && (codeDirPass.length() <2)){

				System.out.println("予期せぬエラーが発生しました1");
				return false;
			}

			File file = new File(codeDirPass, inFileName);
			if( file.exists() ){
				FileReader frCode = new FileReader(file);
				brStore = new BufferedReader(frCode);
			} else {
				System.out.println("支店定義ファイルが存在しません");
				return false;
			}

			String codeLine;
			while((codeLine = brStore.readLine()) != null){
				if(codeLine.matches("^[0-9]{3}+,.*.[^,]$")){
					String[] store = codeLine.split("," );
					branchMap.put(store[0] , store[1]);
					saleMap.put(store[0], 0L);
				} else {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
			}

		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました2");
		} finally {
			if (brStore != null){
				try {
					brStore.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました3");
				}
			}
		}return true;
	}
}